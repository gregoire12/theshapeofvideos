from paletters.cudaKmean import CudaKMeanPaletter
from paletters.paletter import Paletter
from paletters.pyimagesearch_kmean import SKLearnPaletter
from scene_cutter import FrameReducedList


class SamplingPaletter(Paletter):
    def resize_frame(self,frame):
        resized = frame.copy()
        size = list(map(lambda x: int(x / self.scale_factor), frame.size))
        resized.thumbnail(list(size))
        return resized

    def __init__(self, frames, thread_count=4,scale_factor = 5) -> None:
        self.scale_factor =scale_factor
        frames = FrameReducedList(frames.video_capture,frames,self.resize_frame)
        super().__init__(frames,thread_count )

class KMeanSampler(SamplingPaletter,SKLearnPaletter):
    pass
class CudaKMeanSampler(SamplingPaletter,CudaKMeanPaletter):
    pass