from collections import Counter

import cv2
import numpy
from PIL import Image
from libKMCUDA import kmeans_cuda
from sklearn.cluster import KMeans

from paletters.paletter import Paletter


class CudaKMeanPaletter(Paletter):
    def compute_for_frame(self, frame):
        clusters = 7
        pixels = numpy.array(frame,dtype=numpy.float32)
        pixels = pixels.reshape(pixels.shape[0]*pixels.shape[1],3)
        centroids, assignments = kmeans_cuda(pixels, clusters)
        counter = Counter(assignments)
        counts = counter.values()
        s = sum(counts)
        counts = list(map(lambda x: int(x / s * 100), counts))
        s = sum(counts)
        while s != 100:
            counts[0] += 100 - s
            s = sum(counts)
        print("returning pixels")
        return tuple(pixel for color, count in zip(centroids, counts) for pixel in
                     [color.astype("uint8").tolist() for i in range(count)])
    def save_pixels(self, param):
        pass
#
# frame = Image.open("/home/gpeltier/Documents/theshapeofvideos/image/grab.png")
# frame = numpy.array(frame, dtype=numpy.float32)
# frame = frame.reshape(frame.shape[0]*frame.shape[1],3)
# print(frame)
#
# clusters = 7
# centroids, assignments = kmeans_cuda(frame, clusters)
# print(centroids,assignments)
# Image.fromarray(centroids.astype(numpy.uint8).reshape(2,7,3)).save("./test.png")
# clt = KMeans(n_clusters=7)
# image = frame
# clt.fit(image)
# print("treated frame: ",frame)
# counter = Counter(clt.labels_)
# counts = counter.values()
# s = sum(counts)
# counts = list(map(lambda x: int(x / s*100), counts))
# s = sum(counts)
# while s!=100:
#     counts[0]+= 100-s
#     s = sum(counts)
# print("returning pixels")
# classical_center = clt.cluster_centers_
# Image
# t =  tuple(pixel for color, count in zip(clt.cluster_centers_, counts) for pixel in
#       [color.astype("uint8").tolist() for i in range(count)])
