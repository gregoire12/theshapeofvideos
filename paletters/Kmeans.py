import random
from math import sqrt

from tqdm import tqdm

from paletter import Paletter


class Point:

    def __init__(self, coordinates):
        self.coordinates = coordinates

class Cluster:

    def __init__(self, center, points):
        self.center = center
        self.points = points

def euclidean(p, q):
    n_dim = len(p.coordinates)
    return sqrt(sum([
        (p.coordinates[i] - q.coordinates[i]) ** 2 for i in range(n_dim)
    ]))
class KMeanPalleter(Paletter):

    def __init__(self, frames) -> None:
        super().__init__(frames)

    def get_points(self,img):
        img = img.convert("RGB")
        w, h = img.size
        points = []
        for count, color in img.getcolors(w * h):
            for _ in range(count):
                points.append(Point(color))
        return points
    @property
    def palletes(self):
        for img in tqdm(self.frames):
            points = self.get_points(img)
            clusters = KMeans(n_clusters=7).fit(points)
            clusters.sort(key=lambda c: len(c.points), reverse=True)
            rgbs = [map(int, c.center.coordinates) for c in clusters]
            print("got one",rgbs)
            yield list(rgbs)

#%%
class KMeans:

    def __init__(self, n_clusters, min_diff=1):
        self.n_clusters = n_clusters
        self.min_diff = min_diff

    def calculate_center(self, points):
        n_dim = len(points[0].coordinates)
        vals = [0.0 for i in range(n_dim)]
        for p in points:
            for i in range(n_dim):
                vals[i] += p.coordinates[i]
        coords = [(v / len(points)) for v in vals]
        return Point(coords)

    def assign_points(self, clusters, points):
        plists = [[] for i in range(self.n_clusters)]
        print("assigning points")
        for p in tqdm(points):
            smallest_distance = float('inf')

            for i in range(self.n_clusters):
                distance = euclidean(p, clusters[i].center)
                if distance < smallest_distance:
                    smallest_distance = distance
                    idx = i

            plists[idx].append(p)

        return plists

    def fit(self, points):
        clusters = [Cluster(center=p, points=[p]) for p in random.sample(points, self.n_clusters)]
        with tqdm() as pbar:
            while True:

                plists = self.assign_points(clusters, points)

                diff = 0

                for i in range(self.n_clusters):
                    if not plists[i]:
                        continue
                    old = clusters[i]
                    center = self.calculate_center(plists[i])
                    new = Cluster(center, plists[i])
                    clusters[i] = new
                    diff = max(diff, euclidean(old.center, new.center))
                print('diff, ',diff)
                if diff < self.min_diff:
                    break
                pbar.update(1)
        return clusters
