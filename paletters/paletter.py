import colorsys
import json
import pickle
from multiprocessing.pool import Pool

import colorgram
import numpy
import math
from PIL import Image
from tqdm import tqdm

import progress_logger


class Paletter:

    def __init__(self, frames, thread_count = 2) -> None:
        super().__init__()
        self.palette_object = None
        self.frames = frames
        self._pallets = {}
        self.picture =  None
        self.thread_count = thread_count
    @property
    def palletes(self):
        for frame in self.frames:
            yield self.get_palet(frame)

    def get_palet(self, frame):
        for_frame = self.compute_for_frame(frame)
        return for_frame


    def tohls(self,x):
        try:
            return colorsys.rgb_to_hls(*x)
        finally:
            return x

    def get_image(self):
        p = Pool(self.thread_count)
        print("getting images")
        rows = p.map(self.get_palet, tqdm(self.frames))
        p.close()
        pixels = [list(map(lambda x:tuple(x)+(255,),sorted(row,key=self.tohls, reverse=True)) ) for row in rows]
        self.pixels = pixels
        p.close()
        #Iris radius in pixel
        iris_radius=2000
        pupil_ratio=1/4
        data = numpy.zeros((2*iris_radius, 2*iris_radius, 3), dtype=numpy.uint8)

        #pixels dimensions
        nb_pal,len_pal = len(pixels),len(pixels[0])
        progress_logger.log("Iris Generation","Starting")
        print("Iris generation")
        with tqdm(total=2*iris_radius) as pbar:
            for x in range(2*iris_radius):
                pbar.update(1)
                progress_logger.log("Iris Generation ",str(pbar))
                for y in range(2*iris_radius):
                    dist=math.sqrt((x-iris_radius)**2+(y-iris_radius)**2)/iris_radius
                    if dist>1 or dist<pupil_ratio:
                        continue
                    agl=math.atan2(x-iris_radius,y-iris_radius)+math.pi/2
                    agl=agl if agl>0 else agl+2*math.pi
                    pal_choice=min(nb_pal-1,int(agl*nb_pal/(2*math.pi)))
                    col_choice=min(len_pal-1, int((dist-1/4)*len_pal/(3/4)))
                    data[x,y]=pixels[pal_choice][col_choice][:3]

        self.picture = Image.fromarray(data, 'RGB')
        return self.picture

        #return Image.fromarray(numpy.array(
        #    pixels,dtype=numpy.uint8),).rotate(90,expand=True).transpose(Image.FLIP_TOP_BOTTOM)
    def dump(self):
        """:return dump data of the computed pallets"""
        if self.palette_object:
            return pickle.dumps(self.palette_object)
        else :
            return pickle.dumps(self.pixels)
    def load_dump(self,bytes):
        """:return the unpickled data, doesn't handle loading to the right field"""
        return pickle.loads(bytes)
    def compute_for_frame(self, frame):
        return colorgram.colorgram.extract(frame, 7)
