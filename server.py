import io
import os
import shutil
from time import sleep
from uuid import uuid4

import firebase_admin
import numpy
import requests
from firebase_admin import firestore, storage, credentials

import progress_logger
import settings
from directory_runner import FileRunner
from paletters.pyimagesearch_kmean import SKLearnPaletter
from paletters.sampling_paletter import KMeanSampler

class Downloader:
    def __init__(self, storage) -> None:
        print(storage)
        self.link = storage["link"]
        self.name = storage["name"]

    def download(self, dir):
        try:
            os.makedirs(dir)
        except:
            pass



class GDriveDownloader(Downloader):
    import requests
    def __init__(self, storage) -> None:
        super().__init__(storage)


    @property
    def id(self):
        return self.link.split('id=')[-1]
    def download(self,dir):
        super().download(dir)
        target = os.path.join(dir,self.name)
        print(target)
        URL = "https://docs.google.com/uc?export=download"

        session = requests.Session()
        id = self.id
        print(id)
        response = session.get(URL, params={'id': id}, stream=True)
        token = self.get_confirm_token(response)

        if token:
            params = {'id': id, 'confirm': token}
            response = session.get(URL, params=params, stream=True)

        self.save_response_content(response, target)
        return target

    def get_confirm_token(self,response):
        for key, value in response.cookies.items():
            if key.startswith('download_warning'):
                return value

        return None

    def save_response_content(self,response, destination):
        CHUNK_SIZE = 32768
        try:
            os.makedirs(os.path.dirname(destination))
        except:
            pass
        with open(destination, "wb") as f:
            for chunk in response.iter_content(CHUNK_SIZE):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

class LinkDirectDownloader(Downloader):

    def download(self, dir):
        super().download(dir)

        # NOTE the stream=True parameter below
        target = os.path.join(dir, self.name)
        with requests.get(self.link, stream=True) as r:
            r.raise_for_status()
            with open(target, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                        # xf.flush()
            r.close()
        return target


class FirebaseStorageService:

    def __init__(self) -> None:
        super().__init__()
        self.bucket = storage.bucket()

    def uploadPictureBytes(self, picture, target_path):
        blob = self.bucket.blob(target_path)
        print("upload from file")
        blob.upload_from_file(picture, True, content_type="image/jpg")
        print("uploaded")
        return blob.name
    def uploadPureBytes(self,bytes,target_path):
        blob = self.bucket.blob(target_path)
        print("upload from file")
        blob.upload_from_file(bytes, True)
        print("uploaded")
        return blob.path




class ShapeServer:

    def __init__(self) -> None:
        super().__init__()

        self.treated = []

    ERROR_STAGE = "error"
    VIDEO_COLLECTION = u"videos"
    DOWNLOAD_STAGE = "downloading"
    COMPUTE_STAGE = "computing"
    DONE_STAGE = "done"
    @property
    def video_collection(self):
        return self.db.collection(self.VIDEO_COLLECTION)
    def start(self):
        self.prepare_client()
        self.listen_to_video()


    def prepare_client(self):
        creds = credentials
        default_app = firebase_admin.initialize_app()
        self.db = firestore.client()

    def listen_to_video(self):
        self.video_collection.on_snapshot(self.handle_video)

    def handle_video(self,snaps,change,timestamp):
        with open("dict",'w') as f:
            for snap in snaps:
                data = snap.to_dict()
                if "stage" not in data or not data["stage"]:
                    progress_logger._currentLogger = progress_logger.FirestoreProgressLogger(data["name"], "")
                    try:
                        try:
                            self.update_stage(self.DOWNLOAD_STAGE,snap)
                            progress_logger.log("Downloading","InProgress")
                            path = self.download_video(data)
                            # path = "./video/themis.mp4"
                            self.update_stage(self.COMPUTE_STAGE,snap)
                            runner = self.compute_video(path)
                            self.treated.append((snap,runner))
                            self.update_palettes(snap,runner)
                        except Exception as e:
                            self.update_stage(self.ERROR_STAGE,snap)
                            new_data = snap.reference.get().to_dict()
                            new_data.update({"error": str(e)})
                            snap.reference.set(new_data)
                        finally:

                            self.cleanup(snap)
                    except Exception as e:
                        self.update_stage(self.ERROR_STAGE,snap)
                        new_data = snap.reference.get().to_dict()
                        new_data.update({"error_on_cleanup":str(e)})
                        snap.reference.set(new_data)


    def download_video(self, snap):
        print("donloading")
        storage = snap['storage']
        storer = storage["storage_type"]
        if storer == "GoogleDrive":
            downloader = GDriveDownloader( storage)
        elif storer == "Local":
            return storage['link']
        else:
            downloader = LinkDirectDownloader(storage)
        path = downloader.download("./video")

        return path


    def compute_video(self, path):
        runner = FileRunner(KMeanSampler,[],paletter_kwargs={"thread_count":6,"scale_factor":10}, frame_spacing=120)
        runner.run(path)
        return runner

    def update_palettes(self, snap, runner):
        data = snap.to_dict()

        if "palettes" not in data:
            data["palettes"] = []

        if "pictures" not in data:
            data["pictures"] = []
        palettes = data["palettes"]
        pictures = data["pictures"]
        uploader = FirebaseStorageService()
        for p in runner.palletters:
            uuid_ = str(uuid4())
            b = io.BytesIO()
            b.write(p.dump())
            dump_path = uploader.uploadPureBytes(b,target_path=f"dumps/{uuid_}.png")
            palettes.append({
                "id": uuid_,
                "dump":dump_path,
                "version":settings.VERSION,
            })
            print('to bytes')
            b = io.BytesIO()
            p.picture.save(b, "JPEG")
            # try:
            #     p.picture.save(f"./shapes/{uuid_}.png",)
            # finally:
            #     pass
            print("Uploading")
            file_location = uploader.uploadPictureBytes(b, f"shapes/{uuid_}.jpg")
            pictures.append({
                "palettes":uuid_,
                "file":file_location,
            })
        data["stage"] = self.DONE_STAGE
        snap.reference.set(data)

    def cleanup(self, snap):
        try:
            shutil.rmtree("./video",)
        except Exception as e:
            print("we could not find the video file")
        finally:
            pass

    def update_stage(self, stage, snap):
        data = snap.to_dict()
        data.update({"stage": stage})
        print("data",data)
        snap.reference.set(data)


if __name__ == '__main__':
    server = ShapeServer()
    server.start()
    while True:
        sleep(5)
