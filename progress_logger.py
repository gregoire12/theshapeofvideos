import time

import firebase_admin
from firebase_admin import firestore


class ProgressLogger:

    def __init__(self, file, prefix= "") -> None:
        self.file = file
        self.prefix  = prefix

    def update(self,stage,percentage):
        print(f"{self.prefix} {self.file} - {stage} : {percentage}")
class FirestoreProgressLogger(ProgressLogger):
    VIDEO_COLLECTION = u"videos"

    def __init__(self, file, prefix = "") -> None:
        super().__init__(file, prefix)
        self.db = firestore.client()
        self.stage = None
        self.last_time = 0
    def update(self, stage, percentage):
        updatable = self.stage != stage or (self.rate_ok())
        if updatable:
            self.db.collection(self.VIDEO_COLLECTION).document(self.file).update({"stage":f"{self.prefix} {stage} - {percentage}"})
            super().update(stage, percentage)

            self.last_time = time.time()
            self.stage = stage

    def rate_ok(self):
        return time.time() -self.last_time >10


_currentLogger = ProgressLogger("processing", "")
def log(*args,**kwargs):
    _currentLogger.update(*args,**kwargs)
