import os
import time
from os import listdir
from os.path import isfile, join

import numpy
import numpy as np
from PIL import Image

from directory_runner import DirectoryRunner, FileRunner
from paletters.pyimagesearch_kmean import SKLearnPaletter
from paletters.sampling_paletter import KMeanSampler, CudaKMeanSampler
from scene_cutter import SceneCutter
from tqdm import tqdm
from cmd import Cmd

class RunnerMenu(Cmd):

    DIRECTORY_TARGET = "Directory_target"
    FILE_TARGET = "File_target"

    def __init__(self,runnerclass, navigation_target,initial_selection=None, completekey='tab', stdin=None, stdout=None):
        super().__init__(completekey, stdin, stdout)
        self.thread_count = 4
        self.frame_spacing = 30
        self.scale_factor = 5
        self.runnerclass = runnerclass
        self.navigation_target = navigation_target
        self.selection= initial_selection
        self.update_prompt()

    def update_prompt(self):
        self.prompt = f"You are selecting {'a file' if self.navigation_target==self.FILE_TARGET else 'a directory'}\n" \
                      f"Current directory : {os.getcwd()}\n" \
                      f"Current selection : {self.selection}\n" \
                      f"Settings:\n" \
                      f"scale_factor : {self.scale_factor} , frame_spacing : {self.frame_spacing} , thread_count : {self.thread_count} "

    def postcmd(self, stop, line):
        self.update_prompt()
        return super().postcmd(stop, line)
    def do_change_settings(self,arg):
        settings, value = arg.split(" ")
        if settings in {"scale_factor","scale","factor"}:
            self.scale_factor = float(value)
        elif settings in {"frame_spacing","frame","spacing"}:
            self.frame_spacing = float(value)
        elif settings in {"thread_count","thread","count"}:
            self.thread_count = float(value)

    def do_cd(self,target):
        try:
            os.chdir(target)
            self.selection = os.getcwd()
        except:
            print("The given path is not valid")

    def help_validate(self):
        print("Validate choice and launch the image building")
        print(f"Options : \n"
              f"  scene : build the scene by scene color palette \n"
              f"  sample: build the sampled frame by sample frame palette\n"
              f"  if none are set they are  both considered true, otherwise we set the one specified")
    def do_validate(self,arg):
        args = arg.split(" ")
        sample = True
        scene = True
        if "sample" in args and "scene" not in args:
            scene = None
        if "scene"  in args and "sample" not in args:
            sample = None
        stop = False
        if not isfile(self.selection) and self.navigation_target == self.FILE_TARGET:
            print("No files selected did you mean one of the following : ")
            files = [f for f in listdir(self.selection) if isfile(join(self.selection, f))]
            for i,file in enumerate(files):
                print(f"{i}- {file}")
            id = input("File number or q to quit ")
            if id != "q":
                self.selection = join(self.selection, files[int(id)])
            else:
                stop  = True
        if not stop:
            print("Starting treatement")
            print(f"pictures will be produced in subfolder of {os.getcwd()}")
            self.runnerclass(KMeanSampler,[],paletter_kwargs={"thread_count":self.thread_count,"scale_factor":self.scale_factor},frame_spacing=self.frame_spacing,build_sample = sample,build_scene = scene).run(self.selection)


class MainMenu(Cmd):
    prompt = "You are in the main menu\n type dir or file to select a target or ? for help "
    def help_dir(self):
        print("Treats an entire directory, you can specify a starting dir, or it will help you navigate to your choice directory")
        return True
    def do_dir(self,basepath=None):
        if basepath:
            os.chdir(basepath)
        RunnerMenu(DirectoryRunner,RunnerMenu.DIRECTORY_TARGET,basepath).cmdloop()
    def help_file(self):
        print("Treats a file, you can specify a filepath, or it will help you navigate to your desired file")
        return True
    def do_file(self,basepath = None):
        if basepath:
            os.chdir(os.path.dirname(basepath))
        RunnerMenu(FileRunner,RunnerMenu.FILE_TARGET,basepath).cmdloop()



if __name__ == '__main__':
    # runner = FileRunner(KMeanSampler,[10])
    # runner.run("D:\Video\Films\Gattaca.1997.1080p.BrRip.x264.bitloks.YIFY.mp4")
    # MainMenu().cmdloop(intro = "Welcome to the shape of videos")
    start = time.time()
    runner =FileRunner(CudaKMeanSampler,[],paletter_kwargs={"thread_count":2,"scale_factor":10},build_sample=False,build_scene=True)
    runner.run("CloudAtlas/cloud_atlas.mp4")
    runtime = time.time()-start
    print(runtime)
    # #
    # # # video_prefix = "GO"
    # # scene_cutter = SceneCutter("D:\Video\Séries\Good Omens\Episode1.avi")
    # os.chdir("./outs")
    # video_prefix = "Themis"
    # scene_cutter = SceneCutter("D:\Google Drive\Cours\Post-Bac\SI4\PNS INNOV\FinalThemisAds.mp4")
    # frames = scene_cutter.get_scene_starting_frames()
    # paletter = SKLearnPaletter(frames)
    # image = paletter.get_image()
    # image.show()
    # image.save(f"./{video_prefix}_Scenes.png")
    # try:
    #     paletter.save_pixels(video_prefix+"Scenes_pixels")
    # finally:
    #     pass
    # print("Starting Frames Done")
    # print("Now going to the hard part")
    # frames = scene_cutter.sample_frames(240)
    # paletter = SKLearnPaletter(frames)
    # image = paletter.get_image()
    # image.show()
    # try:
    #     paletter.save_pixels(video_prefix+"sample_pixels")
    # finally:
    #     pass
    # image.save(f"./{video_prefix}_Samples.png")
