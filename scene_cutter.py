import numpy
import cv2
from PIL import Image
from scenedetect import VideoManager, SceneManager, ContentDetector
from scenedetect.stats_manager import StatsManager
from tqdm import tqdm

import progress_logger


class SceneCutter:
    def __init__(self, video_path):
        self._timecodes = None
        self._vm = None
        self._stm = None
        self._scm = None
        self.video_path = video_path

    @property
    def video_manager(self):
        if not self._vm:
            self._vm = VideoManager([self.video_path])
        return self._vm

    @property
    def stats_manager(self):
        if not self._stm:
            self._stm = StatsManager()
        return self._stm

    @property
    def scene_manager(self):
        if not self._scm:
            self._scm = SceneManager(self.stats_manager)
        return self._scm

    def extract_timecode(self):
        progress_logger.log("TimeCodes","Started")
        # Add ContentDetector algorithm (constructor takes detector options like threshold).
        self.scene_manager.add_detector(ContentDetector())
        base_timecode = self.video_manager.get_base_timecode()
        start_time = base_timecode
        end_time = base_timecode + self.video_manager.get_duration()[0]
        # Set video_manager duration to read frames from 00:00:00 to 00:00:20.
        self.video_manager.set_duration(start_time=start_time, end_time=end_time)

        # Set downscale factor to improve processing speed (no args means default).
        self.video_manager.set_downscale_factor()

        # Start video_manager.
        self.video_manager.start()

        # Perform scene detection on video_manager.
        self.scene_manager.detect_scenes(frame_source=self.video_manager)

        # Obtain list of detected scenes.
        scene_list = self.scene_manager.get_scene_list(base_timecode)
        # Like FrameTimecodes, each scene in the scene_list can be sorted if the
        # list of scenes becomes unsorted.

        self.video_manager.release()
        progress_logger.log("TimeCodes","Done")
        return scene_list

    def get_scene_starting_frames(self):
        video_capture = self.get_video_capture()
        timecodes = self.timecodes
        timecodes = set(map(lambda x: x[0].frame_num, timecodes))
        return TimecodedFrameList(video_capture, timecodes)

    def get_video_capture(self) -> cv2.VideoCapture:
        return cv2.VideoCapture(self.video_path)

    @property
    def timecodes(self):
        if not self._timecodes:
            self._timecodes = self.extract_timecode()
        return self._timecodes

    def sample_frames(self, rate):
        video_capture = self.get_video_capture()
        return SampleFrameList(video_capture, rate, int(video_capture.get(cv2.CAP_PROP_FRAME_COUNT)))


class FrameList:
    """Iterator that counts upward forever."""

    def __getstate__(self):
        state = self.__dict__.copy()
        state["video_capture"] = None
        return state

    def __init__(self, video_capture):
        self.video_capture = video_capture
        self.count = 0

    def __iter__(self):
        return self
    def _log_progress(self,progress):
        progress_logger.log("FrameIteration",progress)
    def __next__(self):
        video_capture = self.video_capture
        if self.video_capture.get(cv2.CAP_PROP_POS_FRAMES) != self.count:
            self.video_capture.set(cv2.CAP_PROP_POS_FRAMES, self.count)
        success, image = video_capture.read()
        self.count += 1
        if not success:
            raise StopIteration
        r, g, b = Image.fromarray(image, 'RGB').split()

        return Image.merge('RGB', (b, g, r))


class TimecodedFrameList(FrameList):

    def __init__(self, video_capture, timecodes):
        super().__init__(video_capture)
        self.timecodes = timecodes
        self._sorted_timecodes = sorted(timecodes)

    def __next__(self):
        img = super().__next__()
        while self.count not in self.timecodes:
            img = super().__next__()
        self._log_progress(int((self._sorted_timecodes.index(self.count)/len(self.timecodes))*100))
        return img

    def __len__(self):
        return len(self.timecodes)


class SampleFrameList(FrameList):

    def __init__(self, video_capture, rate, total):
        super().__init__(video_capture)
        self.rate = rate
        self.total = total

    def __next__(self):
        if self.count>self.total:
            raise StopIteration
        img = super().__next__()
        while self.count % self.rate:
            img = super().__next__()
        progress = (self.count / self.total) * 100
        self._log_progress(progress)
        return img

    def __len__(self):
        return int(self.total / self.rate)


class FrameReducedList(FrameList):
    def __init__(self, video_capture, sublist, reducer):
        super().__init__(video_capture)
        self.sublist = sublist
        self.reducer = reducer

    def __next__(self):
        image = self.sublist.__next__()
        return self.reducer(image)

    def __len__(self):
        return len(self.sublist)
