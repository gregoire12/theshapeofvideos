import os.path
from os import listdir
from os.path import isfile, join

import progress_logger
from progress_logger import ProgressLogger
from scene_cutter import SceneCutter


class DirectoryRunner:
    def __init__(self, _paletter_class, paletter_args, paletter_kwargs=None, frame_spacing=30, build_sample=True,
                 build_scene=True, ) -> None:
        if paletter_kwargs is None:
            paletter_kwargs = dict()

        self._build_sample = build_sample
        self._build_scene = build_scene
        self._paletter_kwargs = paletter_kwargs
        self._paletter_args = paletter_args
        self._paletter_class = _paletter_class
        self._frame_spacing = frame_spacing
        self.palletters = []
        self.images = []

    def run(self, path):
        files = [f for f in listdir(path) if isfile(join(path, f))]
        print("detected files ", files)
        for file in files:
            print("runnning for file ", file)
            self._run_on_file(file, path)

    def _run_on_file(self, file, path):
        if self._build_scene:
            progress_logger._currentLogger.prefix = "Scenes"
            self.build_scenes(path, file)
        if self._build_sample:
            progress_logger._currentLogger.prefix = "Sampling"
            self.build_sampled(path, file)

    def build_scenes(self, path, file):
        video_prefix = os.path.basename(path) + "/" + file
        scene_cutter = SceneCutter(os.path.join(path, file))
        frames = scene_cutter.get_scene_starting_frames()
        self.build_image(frames, f"./{video_prefix}_Scenes.png", video_prefix + "Scenes_pixels")
        scene_cutter.video_manager.release()

    def build_image(self, frames, image_name, pixel_name):
        paletter = self.init_paletter(frames)

        image = paletter.get_image()
        try:
            os.makedirs(os.path.dirname(image_name))
        except:
            pass
        image.save(image_name)
        try:
            try:
                os.makedirs(os.path.dirname(pixel_name))
            except:
                pass
            paletter.save_pixels(pixel_name)
        finally:
            pass
        self.palletters.append(paletter)
        self.images.append(image)

    def build_sampled(self, path, file):
        video_prefix = os.path.basename(path) + "/" + file
        scene_cutter = SceneCutter(os.path.join(path, file))
        frames = scene_cutter.sample_frames(self._frame_spacing)
        self.build_image(frames, f"./{video_prefix}_Samples.png", video_prefix + "sample_pixels")
        scene_cutter.video_manager.release()

    def init_paletter(self, frames):
        return self._paletter_class(frames, *self._paletter_args, **self._paletter_kwargs)

    def close(self):
        pass


class FileRunner(DirectoryRunner):

    def __init__(self, _paletter_class, paletter_args, paletter_kwargs=None, frame_spacing=30, build_sample=True,
                 build_scene=True):
        super().__init__(_paletter_class, paletter_args, paletter_kwargs=paletter_kwargs, frame_spacing=frame_spacing,
                         build_sample=build_sample,
                         build_scene=build_scene)

    def run(self, path):
        file = os.path.basename(path)
        path = os.path.dirname(path)
        print("runnning for file ", file)
        self._run_on_file(file, path)
